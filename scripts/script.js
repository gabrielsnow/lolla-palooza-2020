const NativeUI = require('NativeUI');
const Scene = require('Scene');
const Diagnostics = require('Diagnostics');
const Textures = require('Textures');
const Patches = require('Patches');
const Re = require('Reactive');
const Shaders = require('Shaders');
const texture0 = Textures.get('icon1');
const texture1 = Textures.get('icon2');
const texture2 = Textures.get('icon3');
const texture3 = Textures.get('icon4');
const seg = Textures.get('personSegmentationMaskTexture0')
const Time = require('Time');
// const value1 = Re.pack2(0,0.5);
// const radio1 = Patches.getScalarValue('radio');
// const screenPosition = Re.pack2();


const picker = NativeUI.picker;
var index = 0;

const configuration = {
  selectedIndex: index,
  items: [
    {image_texture: texture0},
    {image_texture: texture1},
    {image_texture: texture2},
    {image_texture: texture3}
  ]

};
//Diagnostics.watch("Position on Screen : ", )
picker.configure(configuration);
picker.visible = true;
//Diagnostics.watch("timelapsed", Time.ms.div(1000))
Patches.setScalarValue('actualSelectedIndex', picker.selectedIndex);